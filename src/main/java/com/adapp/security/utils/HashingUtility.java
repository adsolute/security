package com.adapp.security.utils;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.CodecSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class HashingUtility {

	private static final Logger LOG = LoggerFactory.getLogger(HashingUtility.class);

	private HashingUtility() {}

	public static String generateSalt() {
		return new BigInteger( 250, new SecureRandom() ).toString( 32 );
	}

	public static String hashPassword(String password, String saltString) {

		byte[] bytes = CodecSupport.toBytes( password );
		byte[] salt = CodecSupport.toBytes( saltString );
		MessageDigest digest = null;

		try {
			digest = MessageDigest.getInstance( "SHA-512" );
		}
		catch ( NoSuchAlgorithmException e ) {
			LOG.error(e.getMessage(), e);
		}

		if ( salt != null ) {
			digest.reset();
			digest.update( salt );
		}
		byte[] hashed = digest.digest( bytes );

		int iterations = 255;
		// iterate remaining number:
		for ( int i = 0; i < iterations; i++ ) {
			digest.reset();
			hashed = digest.digest( hashed );
		}

		return Base64.encodeToString( hashed );
	}

	public static String randomPassword() {

		String saltChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while ( salt.length() < 10 ) {
			int index = rnd.nextInt() * saltChars.length();
			salt.append( saltChars.charAt( index ) );
		}
		String saltStr = salt.toString();
		return saltStr.toLowerCase();
	}
}
