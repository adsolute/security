package com.adapp.security.models;

import java.io.Serializable;
import java.security.Principal;

public class UserPrincipal implements Principal, Serializable {
    private String id;
    private String accountId;
    private String username;
    private String password;
    private String salt;

    public UserPrincipal() {
    }

    @Override
    public String getName() {
        return username;
    }

    public UserPrincipal(String id, String accountId, String username, String password, String salt) {
        this.id = id;
        this.accountId = accountId;
        this.username = username;
        this.password = password;
        this.salt = salt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
