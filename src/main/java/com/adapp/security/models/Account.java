package com.adapp.security.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

@Document("accounts")
public abstract class Account {

	@Id
	private String id;
	private String accountId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public abstract List<GrantedRole> getGrantedRoles();
	public abstract <R> Set<GrantedPermission<R>> getGrantedPermissions();
}
