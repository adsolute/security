package com.adapp.security.models;

public class GrantedPermission<R> implements Granted{
	private String onAccountId;
	private Permission<R> permission;

	@Override
	public String getOnAccountId() {
		return onAccountId;
	}

	@Override
	public void setOnAccountId(String onAccountId) {
		this.onAccountId = onAccountId;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission<R> permission) {
		this.permission = permission;
	}
}
