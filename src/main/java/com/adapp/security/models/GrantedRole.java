package com.adapp.security.models;

public class GrantedRole implements Granted {

	private String onAccountId;
	private String onAccountName;
	private String roleId;
	private String roleName;

	public String getOnAccountId() {
		return onAccountId;
	}

	public void setOnAccountId(String onAccountId) {
		this.onAccountId = onAccountId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getOnAccountName() {
		return onAccountName;
	}

	public void setOnAccountName(String onAccountName) {
		this.onAccountName = onAccountName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
