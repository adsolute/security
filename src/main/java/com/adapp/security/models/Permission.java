package com.adapp.security.models;

public class Permission<R> {
    private R resource;
    private Action action;

    public R getResource() {
        return resource;
    }

    public void setResource(R resource) {
        this.resource = resource;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public enum Action {
        CREATE,
        READ,
        UPDATE,
        DELETE,
        ALL
    }
}
