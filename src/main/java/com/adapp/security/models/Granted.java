package com.adapp.security.models;

public interface Granted {

	String getOnAccountId();

	void setOnAccountId(String onAccountId);
}
