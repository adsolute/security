package com.adapp.security.models;

import com.adapp.security.utils.UTC8Iso8601Deserializer;
import com.adapp.security.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Document("roles")
public class Role<R> {
    @Id
    private String id;
    private String roleId;
    private String name;
    private String description;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime createDate;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime updateDate;
    private Set<Permission<R>> permissions;
    private Map<String, Object> otherProperties = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Set<Permission<R>> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission<R>> permissions) {
        this.permissions = permissions;
    }

    public Map<String, Object> getOtherProperties() {
        return otherProperties;
    }

    public void setOtherProperties(Map<String, Object> otherProperties) {
        this.otherProperties = otherProperties;
    }
}
