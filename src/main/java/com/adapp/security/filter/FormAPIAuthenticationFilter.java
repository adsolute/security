package com.adapp.security.filter;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FormAPIAuthenticationFilter extends FormAuthenticationFilter {

    /**
     * override redirect of default filter `authc` and just throw a 401 error
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response ) throws Exception {
        HttpServletResponse resp = WebUtils.toHttp( response );
        resp.sendError( HttpServletResponse.SC_UNAUTHORIZED );
        return false;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {

        //Always return true if the request's method is OPTIONS
        if(request instanceof HttpServletRequest){
            if(((HttpServletRequest) request).getMethod().toUpperCase().equals("OPTIONS")){
                return true;
            }
        }

        return  super.isAccessAllowed(request, response, mappedValue);
    }
}