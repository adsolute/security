package com.adapp.security.realms;

import com.adapp.security.models.UserPrincipal;
import com.adapp.security.models.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.util.ByteSource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.regex.Pattern;

public class UserCredentialsRealm extends BaseMongoRealm {

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        boolean instanceOfUsernamePasswordToken = authenticationToken instanceof UsernamePasswordToken;
        if(!instanceOfUsernamePasswordToken) {
            throw new AuthenticationException("This realm only supports UsernamePasswordToken");
        }
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;

        if (usernamePasswordToken.getUsername() == null) {
            throw new AuthenticationException("Username cannot be null");
        }

        return findAuthenticationInfo(usernamePasswordToken.getUsername());
    }

    private SimpleAuthenticationInfo findAuthenticationInfo(String username) {
        User user = userDao.getByUsername(username);

        if(user == null) {
            throw new UnknownAccountException(String.format("User %s does not exist", username));
        }

        UserPrincipal userPrincipal = new UserPrincipal(user.getId(), user.getAccountId(), user.getUsername(), user.getPassword(), user.getSalt());

        return new SimpleAuthenticationInfo(
                userPrincipal,
                user.getPassword().toCharArray(),
                ByteSource.Util.bytes(user.getSalt()),
                getName()
        );
    }
}
