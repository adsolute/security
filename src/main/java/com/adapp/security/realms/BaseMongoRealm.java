package com.adapp.security.realms;

import com.adapp.security.dao.AccountDao;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.dao.UserDao;
import com.adapp.security.models.*;
import com.adapp.security.models.Permission.Action;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BaseMongoRealm extends AuthorizingRealm {
    public static final String STRING_PERMISION_FORMAT = "%s:%s:%s";

    UserDao userDao;

    AccountDao accountDao;

    RoleDao roleDao;

    public BaseMongoRealm() {
    }

    public BaseMongoRealm(CacheManager cacheManager) {
        super(cacheManager);
    }

    public BaseMongoRealm(CredentialsMatcher matcher) {
        super(matcher);
    }

    public BaseMongoRealm(CacheManager cacheManager, CredentialsMatcher matcher) {
        super(cacheManager, matcher);
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        List<String> usernames = new ArrayList<>();
        for(Object principalObject : principalCollection.asList()){
            UserPrincipal userPrincipal = (UserPrincipal) principalObject;
            usernames.add(userPrincipal.getUsername());
        }

        List<User> users = userDao.getUsersByUsernames(usernames);
        users.forEach(user -> {
            Account account = accountDao.findById(user.getAccountId(), Account.class);
            processAccountPermissions(account, simpleAuthorizationInfo);
            processAccountRolesAuthorization(account, simpleAuthorizationInfo);
        });
        return simpleAuthorizationInfo;
    }

    private void processAccountPermissions(Account account, SimpleAuthorizationInfo simpleAuthorizationInfo) {
        if(account.getGrantedPermissions() == null) {
            throw new RuntimeException("Account's granted permission cannot be null");
        }
        Iterator<GrantedPermission<Object>> grantedPermissionIterator = account.getGrantedPermissions().iterator();
        while (grantedPermissionIterator.hasNext()) {
            GrantedPermission grantedPermission = grantedPermissionIterator.next();
            simpleAuthorizationInfo
                    .addStringPermission(
                            String.format(STRING_PERMISION_FORMAT,
                                    grantedPermission.getOnAccountId().equals("ALL") ? "*" : grantedPermission.getOnAccountId(),
                                    grantedPermission.getPermission().getResource().toString().equals("ALL") ? "*" : grantedPermission.getPermission().getResource(),
                                    grantedPermission.getPermission().getAction().toString().equals("ALL") ? "*" : grantedPermission.getPermission().getAction())
                    );
        }
    }

    private void processAccountRolesAuthorization(Account account, SimpleAuthorizationInfo simpleAuthorizationInfo) {
        account.getGrantedRoles().forEach(grantedRole -> {
            Role role = roleDao.getRole(grantedRole.getRoleId());
            Iterator<Permission> permissionIterator = role.getPermissions().iterator();
            while (permissionIterator.hasNext()) {
                Permission permission = permissionIterator.next();
                simpleAuthorizationInfo
                        .addStringPermission(
                                String.format(STRING_PERMISION_FORMAT,
                                        grantedRole.getOnAccountId().equals("ALL") ? "*" : grantedRole.getOnAccountId(),
                                        permission.getResource().toString().equals("ALL") ? "*" : permission.getResource(),
                                        permission.getAction().toString().equals("ALL") ? "*" : permission.getAction())
                        );
            }
        });
    }
}
