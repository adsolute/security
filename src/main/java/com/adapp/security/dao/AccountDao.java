package com.adapp.security.dao;

import com.adapp.security.models.Account;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDao extends InheritanceDao {

	public <T extends Account> List<T> getAll () {
		return super.getAll(Account.class);
	}

	public void save(Account account) {
		super.save(account);
	}
}
