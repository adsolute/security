package com.adapp.security.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class InheritanceDao extends BaseDao {

	public <T> List<T> getAllChildClass(Class<T> classType) {
		Query query = new Query();
		query.restrict(classType);
		return mongoTemplate.find(query, classType);
	}

	public <T> T findById(String accountId, Class<T> classType) {
		return mongoTemplate.findOne(Query.query(Criteria.where("accountId").is(accountId)), classType);
	}
}
