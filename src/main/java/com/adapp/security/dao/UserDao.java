package com.adapp.security.dao;

import com.adapp.security.models.User;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.regex.Pattern;

@Repository
public class UserDao extends BaseDao<User>{

	public List<User> getUsersByUsernames(List<String> usernames) {
		Query query = new Query();
		query.addCriteria(Criteria.where("username").in(usernames));
		return mongoTemplate.find(query, User.class);
	}

	public User getByUsername(String username) {
		Query query = new Query(Criteria.where("username").is(Pattern.compile(username)));
		return mongoTemplate.findOne(query, User.class);
	}

	public User save(User user) {
		return super.save(user);
	}

	public boolean exists(String username) {
		Query query = new Query(Criteria.where("username").is(Pattern.compile(username)));
		return super.exists(query, User.class);
	}

	public boolean existsUserId(String userId) {
		Query query = new Query(Criteria.where("userId").is(Pattern.compile(userId)));
		return super.exists(query, User.class);
	}

	public PagedResultSet<User> getPagedResultSetBy(QueryParameters queryParameters) {
		queryParameters.getCustomCriteria().add(Criteria.where("accountId").ne("1"));
		return super.retrieve(queryParameters, User.class);
	}
}
