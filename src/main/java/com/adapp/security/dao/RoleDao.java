package com.adapp.security.dao;

import com.adapp.security.models.Role;
import com.google.common.reflect.TypeToken;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleDao<T> extends BaseDao<Role> {

	public static final String ROLE_ID = "roleId";

	public Role<T> save(Role<T> role) {
		return super.save(role);
	}

	public Role<T> findAndReplace(Role<T> role) {
		return super.findAndReplace(role.getId(), role);
	}

	public List<Role<T>> getAllRoles() {
		TypeToken typeToken = new TypeToken<Role<T>>() {};
		return getAll(typeToken);
	}

	public Role<T> getRole(String roleId) {
		return mongoTemplate.findOne(Query.query(Criteria.where(ROLE_ID).is(roleId)), Role.class);
	}

	public boolean exists(String roleId) {
		return super.exists(ROLE_ID, roleId, Role.class);
	}

	public PagedResultSet<Role<T>> getRolePagedResultSetBy(QueryParameters queryParameters) {
		TypeToken typeToken = new TypeToken<Role<T>>() {};
		return super.retrieve(queryParameters, typeToken);
	}

	public Role<T> getRoleByObjectId(String id) {
		return mongoTemplate.findById(new ObjectId(id), Role.class);
	}
}
