package com.adapp.security.dao;

import com.adapp.security.utils.UTC8Iso8601Deserializer;
import com.adapp.security.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.reflect.TypeToken;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.*;

public class BaseDao<T> {
	MongoTemplate mongoTemplate;

	@Autowired
	@Qualifier("adauthMongoTemplate")
	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	List<T> getAll(Class<T> classType) {
		return mongoTemplate.findAll(classType);
	}

	List<T> getAll(TypeToken<T> typeToken) {
		return (List<T>) mongoTemplate.findAll(typeToken.getRawType());
	}

	public T save (T document) {
		return mongoTemplate.save(document);
	}

	public T findAndReplace(String objectId, T document) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(objectId));
		return mongoTemplate.findAndReplace(query, document, FindAndReplaceOptions.options().upsert());
	}

	public void remove (T document) {
		mongoTemplate.remove(document);
	}


	public boolean exists (String key, Object value, Class<T> classType) {
		return mongoTemplate.exists(Query.query(Criteria.where(key).is(value)), classType);
	}

	public T retrieveById(Class<T> typeClass, String id, String collectionName){
		return mongoTemplate.findById(new ObjectId(id), typeClass, collectionName);
	}

	public T retrieveOneByCriteriaMap(Class<T> typeClass, Map<String, Object> criteria){
		Query query = new Query();
		for(Map.Entry<String, Object> c : criteria.entrySet()){
			query.addCriteria(Criteria.where(c.getKey()).is(c.getValue()));
		}
		return mongoTemplate.findOne(query, typeClass);
	}

	boolean exists (Query query, Class<T> classType) {
		return mongoTemplate.exists(query, classType);
	}

	protected PagedResultSet<T> retrieve(QueryParameters queryParameters, TypeToken<T> typeToken) {
		Query totalCountQuery = buildCriteriaQuery(queryParameters);
		long totalCount = mongoTemplate.count(totalCountQuery, typeToken.getRawType());

		Query query = buildCompleteQuery(queryParameters);
		List<T> resultSet =(List<T>) mongoTemplate.find(query, typeToken.getRawType());

		return createPagedResultSet(queryParameters, (int) totalCount, resultSet);
	}

	public PagedResultSet<T> retrieve(QueryParameters queryParameters, Class<T> classType) {
		Query totalCountQuery = buildCriteriaQuery(queryParameters);
		long totalCount = mongoTemplate.count(totalCountQuery, classType);

		Query query = buildCompleteQuery(queryParameters);
		List<T> resultSet = mongoTemplate.find(query, classType);

		return createPagedResultSet(queryParameters, (int) totalCount, (List<T>) resultSet);
	}

	private PagedResultSet<T> createPagedResultSet(QueryParameters queryParameters, int totalCount, List<T> resultSet) {
		PagedResultSet<T> pagedResultSet = new PagedResultSet<>();
		pagedResultSet.setPage(queryParameters.getPage());
		pagedResultSet.setSize(resultSet.size());
		pagedResultSet.setLimit(queryParameters.getLimit());
		pagedResultSet.setTotalCount(totalCount);
		pagedResultSet.setResults(resultSet);

		return pagedResultSet;
	}

	private Query buildCriteriaQuery(QueryParameters queryParameters) {
		Query query = new Query();

		for (Map.Entry<String, Object> entry : queryParameters.criteria.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			if (value instanceof Collection<?>) {
				query.addCriteria(Criteria.where(key).in((ArrayList<String>) value));
			} else {
				if(queryParameters.isRegex()){
					query.addCriteria(Criteria.where(key).regex(String.valueOf(value), "i"));
				}
				else{
					query.addCriteria(Criteria.where(key).is(value));
				}
			}
		}

		for(DateRange r : queryParameters.getDateRanges()){
			query.addCriteria(Criteria.where(r.getField()).lte(r.getEnd()).gte(r.getStart()));
		}

		for (Criteria criteria : queryParameters.getCustomCriteria()) {
			query.addCriteria(criteria);
		}

		return query;
	}

	protected Query buildCompleteQuery(QueryParameters queryParameters) {
		Query query = buildCriteriaQuery(queryParameters);

		if (queryParameters.getSortField() != null && !queryParameters.getSortField().isEmpty()) {
			query.with(Sort.by(queryParameters.isAscending() ? Sort.Direction.ASC : Sort.Direction.DESC, queryParameters.getSortField()));
		}

		if(queryParameters.getPage() > 0){
			query.skip((queryParameters.getPage() - 1l) * queryParameters.getLimit());
			query.limit(queryParameters.getLimit());
		}

		return query;
	}

	public static class QueryParameters {

		private Map<String, Object> criteria;
		private List<Criteria> customCriteria = new ArrayList<>();
		private int page;
		private int limit;
		private String sortField;
		private boolean ascending;
		private boolean regex;
		private List<DateRange> dateRanges = new ArrayList<>();

		/**
		 * default constructor
		 */
		public QueryParameters() {
			super();
			this.criteria = new HashMap<>();
		}

		public QueryParameters(Map<String, Object> criteria, List<Criteria> customCriteria, int page, int limit, String sortField, boolean ascending, boolean regex, List<DateRange> dateRanges) {
			this.criteria = criteria;
			this.customCriteria = customCriteria;
			this.page = page;
			this.limit = limit;
			this.sortField = sortField;
			this.ascending = ascending;
			this.regex = regex;
			this.dateRanges = dateRanges;
		}

		public Map<String, Object> getCriteria() {
			return criteria;
		}

		public void setCriteria(Map<String, Object> criteria) {
			this.criteria = criteria;
		}

		public List<Criteria> getCustomCriteria() {
			return customCriteria;
		}

		public void setCustomCriteria(List<Criteria> customCriteria) {
			this.customCriteria = customCriteria;
		}

		public int getPage() {
			return page;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

		public String getSortField() {
			return sortField;
		}

		public void setSortField(String sortField) {
			this.sortField = sortField;
		}

		public boolean isAscending() {
			return ascending;
		}

		public void setAscending(boolean ascending) {
			this.ascending = ascending;
		}

		public boolean isRegex() {
			return regex;
		}

		public void setRegex(boolean regex) {
			this.regex = regex;
		}

		public List<DateRange> getDateRanges() {
			return dateRanges;
		}

		public void setDateRanges(List<DateRange> dateRanges) {
			this.dateRanges = dateRanges;
		}
	}

	public static class DateRange{
		private String field;
		@JsonSerialize(using = UTC8Iso8601Serializer.class)
		@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
		private DateTime start;
		@JsonSerialize(using = UTC8Iso8601Serializer.class)
		@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
		private DateTime end;
		public DateTime getStart() {
			return start;
		}
		public void setStart(DateTime start) {
			this.start = start;
		}
		public DateTime getEnd() {
			return end;
		}
		public void setEnd(DateTime end) {
			this.end = end;
		}
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
	}

	/**
	 *
	 * @param <T>
	 */
	public static class PagedResultSet<T> {
		private int page;
		private int size;
		private int totalCount;
		private int limit;
		private List<T> results = new ArrayList<>();

		public int getPage() {
			return page;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public int getTotalCount() {
			return totalCount;
		}

		public void setTotalCount(int totalCount) {
			this.totalCount = totalCount;
		}

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

		public List<T> getResults() {
			return results;
		}

		public void setResults(List<T> results) {
			this.results = results;
		}
	}
}
