package com.adapp.security.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HashingUtilityIntegrationTest {

	@Test
	void hashPassword() {
		String salt = HashingUtility.generateSalt();
		String hashedPassword = HashingUtility.hashPassword("tr!alandt3sting", salt);
		System.out.println(salt);
		System.out.println(hashedPassword);
	}
}